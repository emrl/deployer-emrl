<?php

namespace Deployer;

task('service:restart', function (): void {
    $api_key = getenv('OPALSTACK_API_KEY');
    $app_id = getenv('OPALSTACK_APP_ID');

    if ($api_key && $app_id) {
        fetch(
            'https://my.opalstack.com/api/v1/app/update/',
            'post',
            [
                'Content-Type' => 'application/json',
                'Authorization' => "Token $api_key",
            ],
            json_encode([['id' => $app_id]]),
        );
    }
})
    ->desc('Trigger restart of PHP-FPM service');
