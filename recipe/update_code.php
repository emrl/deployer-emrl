<?php

namespace Deployer;

/**
 * Directory to upload.
 */
set('output_path', '.');

/**
 * rsync config.
 */
set('rsync_config', []);

/**
 * Warm the release directory.
 *
 * First copies previous release files into new release directory. This speeds
 * things up as only updated files are sent over the network.
 *
 * Uses `cp` instead of `rsync` so that files created by the server will remain
 * when they have been set as excluded so the `--delete` flag won't remove them.
 */
task('deploy:warm_release', function (): void {
    if (has('previous_release')) {
        run('cp -aT {{previous_release}}/ {{release_path}}/ || true');
    }
})
    ->desc('Warm the release directory by coping previous release');

/**
 * Update code.
 */
task('deploy:update_code', function (): void {
    upload(get('output_path'), '{{release_path}}', get('rsync_config', []));
})
    ->desc('Update code');
