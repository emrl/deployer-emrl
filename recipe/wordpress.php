<?php

namespace Deployer;

use Deployer\Exception\GracefulShutdownException;
use Deployer\Host\Localhost;

/**
 * WP-CLI executable.
 */
set('bin/wp', 'wp');

/**
 * Path to uploads folder from project root.
 */
set('uploads_path', 'public/app/uploads');

/**
 * Site URL.
 */
set('url', 'http://localhost');

/**
 * Database backup path.
 */
set('database_backup_path', '{{deploy_path}}/backups');

/**
 * Temporary database file.
 */
set('database_file', function (): string {
    $path = rtrim(get('database_backup_path'), '/');

    return "$path/database.sql";
});

/**
 * Current time.
 */
set('current_time', fn (): string => date('YmdHis'));

/**
 * Confirm.
 */
task('push:confirm', function (): void {
    writeln('Preparing to push content to <comment>{{hostname}}</>');

    if (!askConfirmation('Continue?')) {
        throw new GracefulShutdownException('<comment>Aborting</>');
    }
})
    ->once()
    ->hidden();

/**
 * Update theme roots.
 */
task('theme:roots', function (): void {
    if (!(currentHost() instanceof Localhost)) {
        cd('{{current_path}}');
    }

    if (test('{{bin/wp}} core is-installed')) {
        run('{{bin/wp}} theme roots reset');
    }
})
    ->desc('Delete theme roots transient');

/**
 * Create backup directory.
 */
task('db:backup:path', function (): void {
    run('mkdir -p {{database_backup_path}}');
})->hidden();

/**
 * Backup database.
 */
task('db:backup', function (): void {
    if (!(currentHost() instanceof Localhost)) {
        cd('{{current_path}}');
    }

    $path = rtrim(get('database_backup_path'), '/');

    run("{{bin/wp}} db export $path/database-{{current_time}}.sql");
})
    ->desc('Backup database')
    ->addBefore('db:backup:path');

/**
 * Pull database.
 */
task('db:pull', function (): void {
    // Change to current path for WP commands
    cd('{{current_path}}');

    // Backup file remotely
    run('{{bin/wp}} db export {{database_file}}');

    // Create reference to local file path to be set next
    $local_file = '';

    on(host('localhost'), function () use (&$local_file): void {
        // Create local backup directory if not exists
        invoke('db:backup:path');

        // Set the local file path
        $local_file = get('database_file');
    });

    // Download the exported file
    download('{{database_file}}', $local_file);

    // Delete the remote exported file
    run('rm {{database_file}}');

    on(host('localhost'), function (): void {
        // Import the file locally
        run('{{bin/wp}} db import {{database_file}}');

        // Delete the local export file
        run('rm {{database_file}}');
    });

    // Get reference to remote URL
    if ($remote_url = get('url')) {
        on(host('localhost'), function () use ($remote_url): void {
            $local_url = get('url');
            $remote_json_url = trim(json_encode($remote_url), '"');
            $local_json_url = trim(json_encode($local_url), '"');

            // Replace the remote URL with local
            run("{{bin/wp}} search-replace $remote_url $local_url --all-tables --precise");
            run("{{bin/wp}} search-replace $remote_json_url $local_json_url --all-tables --precise");
        });
    }

    on(host('localhost'), function (): void {
        // Update theme roots
        invoke('theme:roots');
    });
})
    ->desc('Pull database')
    ->once()
    ->addBefore('db:backup:path');

/**
 * Push database.
 */
task('db:push', function (): void {
    // Change to current path for WP commands
    cd('{{current_path}}');

    // Create reference to local file path to be set next
    $local_file = '';
    $local_url = '';

    on(host('localhost'), function () use (&$local_file, &$local_url): void {
        // Create local backup directory if not exists
        invoke('db:backup:path');

        // Export local file
        run('{{bin/wp}} db export {{database_file}}');

        // Set the local file path
        $local_file = get('database_file');
        $local_url = get('url');
    });

    // Upload export file to remote
    upload($local_file, '{{database_file}}');

    on(host('localhost'), function (): void {
        // Delete the local export file
        run('rm {{database_file}}');
    });

    // Import the file remotely
    run('{{bin/wp}} db import {{database_file}}');

    // Delete the remote file
    run('rm {{database_file}}');

    // Replace the local URL with remote
    if ($remote_url = get('url')) {
        $local_json_url = trim(json_encode($local_url), '"');
        $remote_json_url = trim(json_encode($remote_url), '"');

        run("{{bin/wp}} search-replace $local_url $remote_url --all-tables --precise");
        run("{{bin/wp}} search-replace $local_json_url $remote_json_url --all-tables --precise");
    }
})
    ->desc('Push database')
    ->addBefore('db:backup')
    ->addBefore('push:confirm')
    ->addAfter('theme:roots');

/**
 * Pull media.
 */
task('media:pull', function (): void {
    $path = trim(get('uploads_path'), '/');

    download("{{deploy_path}}/shared/$path/", "$path/", get('rsync_config', []));
})
    ->desc('Pull media')
    ->once();

/**
 * Push media.
 */
task('media:push', function (): void {
    $path = trim(get('uploads_path'), '/');

    upload("$path/", "{{deploy_path}}/shared/$path/", get('rsync_config', []));
})
    ->desc('Push media')
    ->addBefore('push:confirm');

/**
 * Pull database and media.
 */
task('content:pull', [
    'db:pull',
    'media:pull',
])
    ->desc('Pull database and media')
    ->once();

/**
 * Push database and media.
 */
task('content:push', [
    'db:push',
    'media:push',
])
    ->desc('Push database and media');
