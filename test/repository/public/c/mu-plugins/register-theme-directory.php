<?php
/**
 * Plugin Name: Register Theme Directory
 * Description: Register the default Wordpress theme directory
 * Author: EMRL
 * Author URI: http://emrl.com
 * Version: 1.0.0
 */

if (!defined('WP_DEFAULT_THEME')) {
    register_theme_directory(ROOT_DIR);
    register_theme_directory(ABSPATH.'wp-content/themes');
}
