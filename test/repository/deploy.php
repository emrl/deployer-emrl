<?php

namespace Deployer;

set_include_path(getenv('HOME').'/.composer/vendor'.PATH_SEPARATOR.get_include_path());
require_once 'recipe/common.php';
require_once 'emrl/deployer-emrl/recipe/common.php';
require_once 'emrl/deployer-emrl/recipe/wordpress.php';

// Configuration
set('url', getenv('DEV_HOST') ?: get('url'));
set('shared_dirs', ['public/c/uploads']);
set('shared_files', ['public/wp-config.php']);
set('uploads_path', 'public/c/uploads');

// Hosts
localhost()
    ->set('database_backup_path', '/tmp');

host('emrl.dev')
    ->setRemoteUser('deploy')
    ->setDeployPath('~/webapps/deployer-emrl')
    ->setLabels(['stage' => 'staging'])
    ->set('keep_releases', 2)
    ->set('bin/wp', '{{bin/php}} ~/bin/wp')
    ->set('url', 'https://deployer-emrl.emrl.dev');

// Tasks
task('deploy', [
    'deploy:setup',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:clear_paths',
    'deploy:symlink',
    'theme:roots',
    'deploy:cleanup',
    'deploy:unlock',
    'deploy:success',
])
    ->desc('Deploy application');

after('deploy:failed', 'deploy:unlock');
